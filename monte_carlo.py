#!/usr/bin/env python
import numpy as np
import math
import copy
import sys
import json

#Global variables
#Number of photons firing
NPHOTONS = 1000000
#Weight minimum before a photon is considered absorbed
WEIGHT_MIN = .01
#Single scatter albedo
SSA = 1.0
#Asymmetry parameter
G = 0.85
#To change degrees to radians
PI = np.pi
DEG = PI / 180.00
#Optical depth
TAU_STAR = 1.0
#Starting theta and phi of the sun viewing angle
THETA_NOT = 60 * DEG
PHI_NOT = 0* DEG

#Class photon can create a photon, move it around a cloud, absorb weight, and has an "in cloud" boolean to tell if its optical depth, which is the effective z coordinate, is out of the cloud (> or < tau star)
class photon:
	global TAU_STAR
	global PI
	global G
	global SSA
	global WEIGHT_MIN
	#Create a photon with an optical depth, location unit vector, single scatter albedo, angle, weight, boolean signifying whether scattered or not in the past, and booleans for reflected, transmitted, diffused, absorbed
	#Weight is -1 if exit from base, -2 if exit from top
	def __init__(self, theta, phi):
		self.tau = 0
		self.k = (math.cos(phi)*math.sin(theta), math.sin(phi)*math.sin(theta), math.cos(theta) * -1)
		self.single_scatter = SSA
		self.weight = 1.0
		self.scattered = False
		self.reflected = False
		self.transmitted = False
		self.diffused = False
		self.absorbed = False
		self.theta = theta
		self.phi = phi
		self.in_cloud = True
	#Move method checks to make sure the weight is not below threshold
	def move(self):
		if self.weight < WEIGHT_MIN:
			self.absorbed = True
			self.in_cloud = False
			return
		#Finds the new tau using a random number generator
		r = np.random.rand()
		delta_tau = -1* self.k[2] * ((np.log(1/(1-r))))
		self.tau = self.tau + delta_tau
		#Checks to see if in cloud still
		if self.tau > TAU_STAR:
			if self.scattered:
				#print "diffused" 
				self.diffused = True
				self.in_cloud = False
				
			else:
				#print "transmitted"
				self.transmitted = True
				self.in_cloud = False
				
		elif self.tau < 0:
			self.reflected = True
			self.in_cloud = False
		#Otherwise if the photon is in the cloud, calls scatter to scatter the photon
		else:
			self.scatter()
	#Scatters the photon
	def scatter(self):
		self.scattered = True
		r1 = np.random.rand()
		r2 = np.random.rand()
		theta_new = self.scatter_theta(r1)
		phi_new = r2 * 2*PI
		self.scatter_photon(theta_new, phi_new)
		self.weight = self.weight * self.single_scatter
		
	#To find the new theta for scattering
	def scatter_theta(self, r):
		if G != 0:
			temp = (1/(2*G))*(1+G**2 - ((1-G**2)/(1-G+2*G*r))**2)
		else:
			temp = 1 - 2*r
		scatter_theta = np.arccos(temp)
		return scatter_theta
	#To find the new k vector of the photon
	def scatter_photon(self,theta_scat, phi_scat):
		self.scattered = True
		if self.k[1] != 0 and self.k[0] != 0:
			cross_mag = math.sqrt(self.k[1]**2 + self.k[0]**2)
			xp = (-1*self.k[1]/cross_mag, self.k[0]/cross_mag, 0)
		else:
			xp = (-1*math.sin(self.theta), math.cos(self.theta), 0)
		
		kp = (math.sin(theta_scat)*math.cos(phi_scat), math.sin(theta_scat)*math.sin(phi_scat), math.cos(theta_scat))
		one = xp[0]*kp[0] - self.k[2]*kp[1]*xp[1] + self.k[0]*kp[2]
		two = xp[1]*kp[0]+self.k[2]*xp[0]*kp[1]+ self.k[1]*kp[2]
		three = kp[1]*self.k[0]*xp[1]-kp[1]*self.k[1]*xp[0]+self.k[2]*kp[2]
		magnitude = math.sqrt(one**2 + two**2 + three**2)
		k_new = (one/magnitude, two/magnitude, three/magnitude)
		self.k = k_new
		return
		
		
#Creates a photon for every NPHOTON, tracks diffusion, reflected, absorbed, transmmitted	
def fire_away(NPHOTONS):
	diffusion = 0.0
	reflected = 0.0
	absorbed = 0.0
	transmitted = 0.0
	for n in range(NPHOTONS):
		sunlight = photon(THETA_NOT, PHI_NOT)
		while sunlight.in_cloud == True:
			sunlight.move()
		#print sunlight.weight
		if sunlight.reflected:
			reflected += sunlight.weight
			absorbed += (1-sunlight.weight)
		if sunlight.diffused:
			diffusion += sunlight.weight
			absorbed += (1-sunlight.weight)
		if sunlight.transmitted:
			transmitted += sunlight.weight
		if sunlight.absorbed:
			absorbed += 1.0
	return diffusion, reflected, absorbed, transmitted
	

	
	
	
	
		
			
			
			
			
			
		

		
